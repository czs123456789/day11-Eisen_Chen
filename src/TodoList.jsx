import { useState } from "react"
import TodoGenerator from "./TodoGenerator"
import TodoGroup from "./TodoGroup"

const TodoList = () => {
    const [texts, setText] = useState([])

    return(
        <div>
            <TodoGroup texts={texts}/>
            <TodoGenerator  setText={setText}/>
        </div>
    )
}


export default TodoList