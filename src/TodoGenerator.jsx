import "./TodoGenerator.css"
const TodoGenerator = (props) => {
    let inputValue;

    const handlePost = () => {
        if(inputValue.value !== '')
            props.setText(current => [...current,inputValue.value])
    }

    return (
        <div>
            <input ref={input => inputValue=input}/>
            <button onClick={handlePost}>add</button>
        </div>
    )

}

export default TodoGenerator