import TodeItem from './TodoItem'
const TodoGroup = (props)=> {
    const list = props.texts.map(item => {
        return <TodeItem key={item} item={item}/>
    })

    return(
        <div className='TodoGroup'>
            {list}
        </div>
    )
}


export default TodoGroup